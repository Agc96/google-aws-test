INSERT INTO con_usuario (usuario, contrasenia, nombres, correo, tipo, estado, usuario_creacion, fecha_creacion) VALUES ('admin', NULL, 'Administrador', NULL, 1, 1, 'admin', CURRENT_TIMESTAMP());
INSERT INTO con_usuario (usuario, contrasenia, nombres, correo, tipo, estado, usuario_creacion, fecha_creacion) VALUES ('otheo', NULL, 'Otto Theo', 'otheo@pucp.edu.pe', 2, 1, 'admin', CURRENT_TIMESTAMP());
INSERT INTO con_usuario (usuario, contrasenia, nombres, correo, tipo, estado, usuario_creacion, fecha_creacion) VALUES ('ID', NULL, 'Anthony Gutiérrez', 'anthony.gutierrez@pucp.pe', 3, 1, 'admin', CURRENT_TIMESTAMP());

INSERT INTO con_proyecto (nombre, estado, usuario_creacion, fecha_creacion) VALUES ('Gestión de Concursos de Profonanpe', 1, 'admin', CURRENT_TIMESTAMP());
INSERT INTO con_proyecto (nombre, estado, usuario_creacion, fecha_creacion) VALUES ('Registro de Aplicaciones de Productos', 1, 'admin', CURRENT_TIMESTAMP());
INSERT INTO con_proyecto (nombre, estado, usuario_creacion, fecha_creacion) VALUES ('Gestión de Proyectos de TI', 1, 'admin', CURRENT_TIMESTAMP());
INSERT INTO con_proyecto (nombre, estado, usuario_creacion, fecha_creacion) VALUES ('Solicitudes de Requerimientos', 1, 'admin', CURRENT_TIMESTAMP());

INSERT INTO con_postulante (nombre, estado, usuario_creacion, fecha_creacion) VALUES ('JAS Solutions', 1, 'admin', CURRENT_TIMESTAMP());
INSERT INTO con_postulante (nombre, estado, usuario_creacion, fecha_creacion) VALUES ('QTC Team', 1, 'admin', CURRENT_TIMESTAMP());
INSERT INTO con_postulante (nombre, estado, usuario_creacion, fecha_creacion) VALUES ('DSB Mobile', 1, 'admin', CURRENT_TIMESTAMP());
