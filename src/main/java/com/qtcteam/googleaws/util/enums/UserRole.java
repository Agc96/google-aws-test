package com.qtcteam.googleaws.util.enums;

/**
 * Enumerado con los roles de un usuario de esta aplicación.
 * @author Anthony Gutiérrez
 */
public enum UserRole {

    ADMIN(1, "Administrador"),
    RESPONSIBLE(2, "Responsable"),
    APPLICANT(3, "Postulante"),
    INTERNAL_EVALUATOR(4, "Evaluador interno"),
    EXTERNAL_EVALUATOR(5, "Evaluador externo");

    private final int code;
    private final String name;

    private UserRole(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }
    public String getName() {
        return name;
    }

}
