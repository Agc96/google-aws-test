package com.qtcteam.googleaws.util.enums;

/**
 * Enumerado con los tipos de usuario que maneja esta aplicación.
 * @author Anthony Gutiérrez
 */
public enum UserType {

    COMMON(1, "Usuario del sistema"),
    GOOGLE(2, "Usuario de Google");

    private static final UserType[] VALUES = values();

    private final int code;
    private final String name;

    private UserType(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public UserType[] getValues() {
        return VALUES;
    }

    public UserType find(Integer code) {
        if (code == null) return null;
        for (UserType type : VALUES) {
            if (type.code == code) return type;
        }
        return null;
    }

    /// <editor-fold defaultstate="collapsed" desc="Getters">
    public int getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    /// </editor-fold>

}
