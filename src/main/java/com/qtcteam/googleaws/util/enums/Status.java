package com.qtcteam.googleaws.util.enums;

/**
 * Enumerado con los estados de una entidad de base de datos.
 * @author Otto Theo, Anthony Gutiérrez
 */
public enum Status {

    INACTIVE(0, "Inactivo"),
    ACTIVE(1, "Activo");

    private final int code;
    private final String name;

    private Status(int code, String name) {
        this.code = code;
        this.name = name;
    }

    /// <editor-fold defaultstate="collapsed" desc="Getters">
    public int getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    /// </editor-fold>

}
