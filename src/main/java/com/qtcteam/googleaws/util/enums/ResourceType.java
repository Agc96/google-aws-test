package com.qtcteam.googleaws.util.enums;

/**
 * Enumerado con los tipos de recurso de Amazon S3 que maneja esta aplicación.
 * @author Anthony Gutiérrez
 */
public enum ResourceType {

    /** Recurso adjuntado por una empresa postulante a un proyecto. */
    APPLICANT_FILE(1, "Archivo del postulante", "project/%d/applicant/%d/%s"),

    /** Recurso asociado a las bases de un proyecto. */
    BASE_FILE(2, "Bases del proyecto", "project/%d/base/%s"),

    /** Recurso estático para la aplicación Web. */
    STATIC_FILE(3, "Recurso estático", "static/%s"),

    /** Recurso asociado a los anexos de un proyecto. */
    ANNEX_FILE(4, "Anexos del proyecto", "project/%d/annex/%s");

    private static final ResourceType[] VALUES = values();

    private final int code;
    private final String name;
    private final String format;

    private ResourceType(int code, String name, String format) {
        this.code = code;
        this.name = name;
        this.format = format;
    }

    public static ResourceType[] getValues() {
        return VALUES;
    }

    public static ResourceType find(Integer code) {
        if (code == null) return null;
        for (ResourceType type : VALUES) {
            if (type.code == code) return type;
        }
        return null;
    }

    /// <editor-fold defaultstate="collapsed" desc="Getters">
    public int getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public String getFormat() {
        return format;
    }
    /// </editor-fold>

}
