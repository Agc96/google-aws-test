package com.qtcteam.googleaws.util;

/**
 * Colección de constantes para el proyecto.
 * @author Anthony Gutiérrez
 */
public final class Constants {

    /** Mensaje de error indicando un error 404 (página no encontrada). */
    public static final String ERROR_NOT_FOUND = "No se encontró la página solicitada.";

    /** Mensaje de error indicando un error 403 (prohibido). */
    public static final String ERROR_FORBIDDEN = "No tiene los permisos requeridos para acceder a esta página.";

    /** Mensaje de error indicando un error desconocido. */
    public static final String ERROR_UNKNOWN = "Hubo un error desconocido en el servidor.";

    /** Clave para manejar el inicio de sesión con Google. */
    public static final String GOOGLE_CLIENT_ID = System.getenv("GoogleClientId");

    /** Nombre del bucket de Amazon S3 asociado a esta aplicación. */
    public static final String S3_BUCKET_NAME = "google-aws-test";

    /** Nombre del estándar de codificación de caracteres UTF-8. */
    public static final String UTF_8 = "UTF-8";

    /** Nadie debe ser capaz de instanciar esta clase. */
    private Constants() {}

}
