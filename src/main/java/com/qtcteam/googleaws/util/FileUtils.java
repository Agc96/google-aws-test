package com.qtcteam.googleaws.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;

/**
 * Colección de métodos utilitarios para el manejo de archivos.
 * @author Otto Theo, Anthony Gutiérrez
 */
public final class FileUtils {

    public static String getFilename(MultipartFile file) {
        String filename = file.getOriginalFilename();
        // Si el nombre de archivo es vacío, crear un nombre con la fecha actual
        if (Preconditions.isEmpty(filename)) {
            return Long.toString(System.currentTimeMillis());
        }
        // Caso contrario, extraer el nombre del archivo del resto del path
        int startIndex = filename.replaceAll("\\\\", "/").lastIndexOf('/');
        return filename.substring(startIndex + 1);
    }

    public static byte[] toByteArray(File file) {
        if (file == null) return null;
        try (FileInputStream fis = new FileInputStream(file)) {
            byte[] bytes = new byte[(int) file.length()];
            fis.read(bytes);
            return bytes;
        } catch (IOException ex) {
            return null;
        }
    }

    public static void byteArrayToFile(File file, byte[] bytes) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(bytes);
        }
    }

    /** Nadie debe ser capaz de instanciar esta clase. */
    private FileUtils() {}

}
