package com.qtcteam.googleaws.util.exception;

/**
 *
 * @author Anthony Gutiérrez
 */
public class NotFoundException extends AppException {

    public NotFoundException(String entity) {
        super("No se encontró el " + entity + " especificado.");
    }

}
