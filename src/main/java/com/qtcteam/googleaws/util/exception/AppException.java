package com.qtcteam.googleaws.util.exception;

/**
 *
 * @author Otto Theo, Anthony Gutiérrez
 */
public class AppException extends Exception {

    /**
     * Crea una excepción especificando un mensaje de error.
     * @param message Un mensaje detallado explicando el error.
     */
    public AppException(String message) {
        super(message);
    }

    /**
     * Crea una excepción especificando un mensaje de error y una causa.
     * @param message Un mensaje detallado explicando el error.
     * @param cause La causa del error.
     */
    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Genera un mensaje de error para mostrar al usuario en la aplicación Web.
     * @return Mensaje de error para mostrar al usuario.
     */
    public String getAlertMessage() {
        Throwable cause = getCause();
        if (cause != null) {
            return String.format("%s: %s", getMessage(), cause.getMessage());
        }
        return getMessage();
    }

}
