package com.qtcteam.googleaws.util;

import java.util.Collection;
import java.util.Map;

/**
 * Colección de métodos utilitarios para el manejo de precondiciones.
 * @author Anthony Gutiérrez
 */
public final class Preconditions {

    /**
     * Verifica si una cadena de caracteres está vacía.
     * @param string La cadena de caracteres a verificar.
     * @return {@code true} si la cadena es {@code null} o su longitud es 0,
     * {@code false} de otro modo.
     */
    public static boolean isEmpty(String string) {
        return string == null || string.isEmpty();
    }

    /**
     * Verifica si un arreglo de objetos está vacío.
     * @param array El arreglo de objetos a verificar.
     * @return {@code true} si el arreglo es {@code null} o su tamaño es 0,
     * {@code false} de otro modo.
     */
    public static boolean isEmpty(Object[] array) {
        return array == null || array.length == 0;
    }

    /**
     * Verifica si una colección de objetos está vacía.
     * @param collection Colección de objetos a verificar, generalmente una
     * lista ({@link java.util.List}).
     * @return {@code true} si la colección es {@code null} o no contiene
     * elementos, {@code false} de otro modo.
     */
    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    /**
     * Verifica si un mapa de objetos está vacío.
     * @param map Mapa de objetos a verificar, generalmente una tabla hash
     * ({@link java.util.HashMap}).
     * @return {@code true} si el mapa es {@code null} o no contiene elementos,
     * {@code false} de otro modo.
     */
    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    /** Nadie debe ser capaz de instanciar esta clase. */
    private Preconditions() {}

}
