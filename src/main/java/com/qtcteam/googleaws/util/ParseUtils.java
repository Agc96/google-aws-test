package com.qtcteam.googleaws.util;

/**
 * Colección de métodos utilitarios para la conversión de objetos a tipos de
 * datos comunes.
 * @author Otto Theo, Anthony Gutiérrez
 */
public final class ParseUtils {

    /**
     * Convierte un objeto a un entero ({@link Integer}).
     * @param object El objeto a convertir.
     * @return Un entero con el valor del objeto, o {@code null} si es que el
     * objeto es nulo o no se pudo convertir.
     */
    public static Integer toInteger(Object object) {
        if (object == null) return null;
        if (object instanceof Number) {
            return ((Number) object).intValue();
        }
        try {
            return Integer.valueOf(object.toString());
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    /**
     * Convierte un objeto a un número real de doble precisión ({@link Double}).
     * @param object El objeto a convertir.
     * @return Un double con el valor del objeto, o {@code null} si es que el
     * objeto es nulo o no se pudo convertir.
     */
    public static Double toDouble(Object object) {
        if (object == null) return null;
        if (object instanceof Number) {
            return ((Number) object).doubleValue();
        }
        try {
            return Double.valueOf(object.toString());
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    /**
     * Convierte un número a una cadena de caracteres ({@link String}).
     * @param object El objeto a convertir.
     * @return Una cadena con el valor del objeto, o {@code null} si es que el
     * objeto es nulo.
     */
    public static String toString(Object object) {
        return object == null ? null : object.toString();
    }

    /** Nadie debe ser capaz de instanciar esta clase. */
    private ParseUtils() {}

}
