package com.qtcteam.googleaws.util;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.qtcteam.googleaws.util.exception.AppException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Colección de métodos utilitarios para iniciar sesión usando una cuenta de
 * Google o una cuenta administrada por Google (G Suite).
 * @author Anthony Gutiérrez
 */
public class GoogleUtils {

    private static final Logger LOGGER = Logger.getLogger(GoogleUtils.class.getName());
    private static final GoogleIdTokenVerifier VERIFIER = new GoogleIdTokenVerifier
                .Builder(new NetHttpTransport(), new JacksonFactory())
                .setAudience(Collections.singletonList(Constants.GOOGLE_CLIENT_ID))
                .build();

    /**
     * Verifica el token enviado por el inicio de sesión con Google, y devuelve
     * los datos del usuario que ha iniciado sesión.
     * @param token Token que debe ser validado por Google.
     * @return Los datos del usuario que ha iniciado sesión con Google.
     * @throws AppException si es que el token es nulo o no es válido, o si
     * hubo algún error al validar el token.
     */
    public static Payload validateSignIn(String token) throws AppException {
        if (token == null) {
            throw new AppException("Debe ingresar un token generado por Google.");
        }
        try {
            GoogleIdToken result = VERIFIER.verify(token);
            if (result == null) {
                throw new AppException("El token de Google no es válido.");
            }
            Payload payload = result.getPayload();
            return payload;
        } catch (IOException | GeneralSecurityException ex) {
            LOGGER.log(Level.SEVERE, "Error al validar token de Google", ex);
            throw new AppException("Error al validar token de Google", ex);
        }
    }

    /** Nadie debe ser capaz de instanciar esta clase. */
    private GoogleUtils() {}

}
