package com.qtcteam.googleaws.util;

import com.qtcteam.googleaws.util.exception.AppException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

/**
 * Colección de métodos utilitarios para acceder y manipular los archivos del
 * bucket de Amazon S3 asociado a esta aplicación.
 * @author Anthony Gutiérrez
 */
public final class S3Utils {

    private static final Logger LOGGER = Logger.getLogger(S3Utils.class.getName());
    private static final S3Client CLIENT = S3Client.builder()
            .region(Region.US_EAST_1)
            .build();

    /**
     * Envía un archivo al bucket de Amazon S3 asociado a esta aplicación.
     * @param file Archivo enviado a través del formulario para subir archivos.
     * @param key Nombre del archivo que tendrá en el bucket de Amazon S3.
     * @throws AppException Si hubo un error en la petición o en la conexión
     * con la API de Amazon Web Services.
     */
    public static void upload(MultipartFile file, String key) throws AppException {
        try {
            PutObjectRequest request = PutObjectRequest.builder()
                    .bucket(Constants.S3_BUCKET_NAME)
                    .key(key)
                    .build();
            RequestBody body = RequestBody.fromBytes(file.getBytes());
            CLIENT.putObject(request, body);
        } catch (SdkException | IOException ex) {
            LOGGER.log(Level.SEVERE, "Error al subir archivo en Amazon S3", ex);
            throw new AppException("Error al subir archivo en Amazon S3", ex);
        }
    }

    /**
     * Descarga un archivo del bucket de Amazon S3 asociado a esta aplicación.
     * @param key Nombre del archivo que tendrá en el bucket de Amazon S3.
     * @return Arreglo de bytes que contiene los datos del archivo de Amazon S3.
     * @throws AppException Si hubo un error en la petición o en la conexión
     * con la API de Amazon Web Services.
     */
    public static byte[] download(String key) throws AppException {
        try {
            GetObjectRequest request = GetObjectRequest.builder()
                    .bucket(Constants.S3_BUCKET_NAME)
                    .key(key)
                    .build();
            return CLIENT.getObjectAsBytes(request).asByteArray();
        } catch (SdkException ex) {
            LOGGER.log(Level.SEVERE, "Error al descargar archivo desde Amazon S3", ex);
            throw new AppException("Error al descargar archivo desde Amazon S3", ex);
        }
    }

    /**
     * Elimina un archivo del bucket de Amazon S3 asociado a esta aplicación.
     * @param key Nombre del archivo que figura en el bucket de Amazon S3.
     * @throws AppException Si hubo un error en la petición o en la conexión
     * con la API de Amazon Web Services.
     */
    public static void delete(String key) throws AppException {
        try {
            DeleteObjectRequest request = DeleteObjectRequest.builder()
                    .bucket(Constants.S3_BUCKET_NAME)
                    .key(key)
                    .build();
            CLIENT.deleteObject(request);
        } catch (SdkException ex) {
            LOGGER.log(Level.SEVERE, "Error al eliminar recurso de Amazon S3", ex);
            throw new AppException("Error al eliminar recurso de Amazon S3", ex);
        }
    }

    /** Nadie debe ser capaz de instanciar esta clase. */
    private S3Utils() {}

}
