package com.qtcteam.googleaws.dto;

import com.qtcteam.googleaws.entity.Resource;
import com.qtcteam.googleaws.util.enums.ResourceType;

/**
 *
 * @author Anthony Gutiérrez
 */
public class ResourceDto {

    private Resource data;
    private ResourceType type;

    public Resource getData() {
        return data;
    }
    public void setData(Resource data) {
        this.data = data;
    }

    public ResourceType getType() {
        return type;
    }
    public void setType(ResourceType type) {
        this.type = type;
    }

}
