package com.qtcteam.googleaws.dto;

/**
 *
 * @author Anthony Gutiérrez
 */
public class BaseDto {

    private final boolean error;
    private final String message;

    public BaseDto(boolean error, String message) {
        this.error = error;
        this.message = message;
    }

    public boolean isError() {
        return error;
    }
    public String getMessage() {
        return message;
    }

}
