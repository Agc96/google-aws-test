package com.qtcteam.googleaws.service;

import com.qtcteam.googleaws.dao.ApplicantDao;
import com.qtcteam.googleaws.dao.ProjectDao;
import com.qtcteam.googleaws.dao.ResourceDao;
import com.qtcteam.googleaws.entity.Applicant;
import com.qtcteam.googleaws.entity.Project;
import com.qtcteam.googleaws.entity.Resource;
import com.qtcteam.googleaws.util.enums.ResourceType;
import com.qtcteam.googleaws.util.exception.NotFoundException;
import com.qtcteam.googleaws.dto.ResourceDto;
import com.qtcteam.googleaws.entity.AuditData;
import com.qtcteam.googleaws.util.enums.Status;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Anthony Gutiérrez
 */
@Service
public class ResourceService {

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private ApplicantDao applicantDao;

    @Autowired
    private ResourceDao resourceDao;

    public List<Project> getProjects() {
        return projectDao.findAll();
    }

    public ResourceType[] getResourceTypes() {
        return ResourceType.getValues();
    }

    public List<Applicant> getApplicants() {
        return applicantDao.findAll();
    }

    public List<ResourceDto> getResources() {
        List<Resource> entities = resourceDao.findAll();
        List<ResourceDto> dtos = new ArrayList<>(entities.size());
        entities.forEach(entity -> {
            ResourceDto dto = new ResourceDto();
            dto.setData(entity);
            dto.setType(ResourceType.find(entity.getType()));
            dtos.add(dto);
        });
        return dtos;
    }

    public Project findProjectById(Integer id) throws NotFoundException {
        if (id == null) return null;
        Optional<Project> project = projectDao.findById(id);
        if (project.isPresent()) return project.get();
        throw new NotFoundException("proyecto");
    }

    public Applicant findApplicantById(Integer id) throws NotFoundException {
        if (id == null) return null;
        Optional<Applicant> applicant = applicantDao.findById(id);
        if (applicant.isPresent()) return applicant.get();
        throw new NotFoundException("postulante");
    }

    public ResourceType findResourceType(Integer id) throws NotFoundException {
        ResourceType type = ResourceType.find(id);
        if (type != null) return type;
        throw new NotFoundException("tipo de recurso");
    }

    public Resource findResourceById(Integer id) throws NotFoundException {
        if (id == null) throw new NotFoundException("recurso");
        Optional<Resource> resource = resourceDao.findById(id);
        if (resource.isPresent()) return resource.get();
        throw new NotFoundException("recurso");
    }

    public String makeResourceKey(Resource resource) {
        ResourceType type = ResourceType.find(resource.getType());
        switch (type) {
            case APPLICANT_FILE:
                return String.format(type.getFormat(), resource.getProject().getId(),
                        resource.getApplicant().getId(), resource.getName());
            case BASE_FILE:
            case ANNEX_FILE:
                return String.format(type.getFormat(), resource.getProject().getId(),
                        resource.getName());
            case STATIC_FILE:
                return String.format(type.getFormat(), resource.getName());
            default:
                return null;
        }
    }

    public void saveResource(Resource resource) {
        resource.setStatus(Status.ACTIVE.getCode());
        resource.setAuditData(new AuditData("admin"));
        resourceDao.save(resource);
    }

    public void deleteResource(Resource resource) {
        resourceDao.delete(resource);
    }

}
