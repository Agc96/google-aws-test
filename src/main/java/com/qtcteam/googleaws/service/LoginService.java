package com.qtcteam.googleaws.service;

import com.qtcteam.googleaws.dao.UserDao;
import com.qtcteam.googleaws.entity.AuditData;
import com.qtcteam.googleaws.entity.User;
import com.qtcteam.googleaws.util.enums.Status;
import com.qtcteam.googleaws.util.enums.UserType;
import com.qtcteam.googleaws.util.exception.AppException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Anthony Gutiérrez
 */
@Service
public class LoginService {

    @Autowired
    private UserDao userDao;

    public User findCommonUser(String username) throws AppException {
        return findUser(username, UserType.COMMON.getCode());
    }

    public User findGoogleUser(String googleId) throws AppException {
        return findUser(googleId, UserType.GOOGLE.getCode());
    }

    private User findUser(String username, Integer type) throws AppException {
        if (username == null) {
            throw new AppException("No se especificó el nombre de usuario.");
        }
        return userDao.findFirstByUsernameAndType(username, type);
    }

    public void saveUser(User user) {
        user.setStatus(Status.ACTIVE.getCode());
        user.setAuditData(new AuditData("admin"));
        userDao.save(user);
    }

}
