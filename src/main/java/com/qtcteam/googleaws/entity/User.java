package com.qtcteam.googleaws.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad de base de datos que representa un usuario de esta aplicación.
 * @author Anthony Gutiérrez
 */
@Entity
@Table(name = "con_usuario")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Integer id;

    @Column(name = "usuario")
    private String username;

    @Column(name = "contrasenia")
    private String password;

    @Column(name = "nombres")
    private String fullname;

    @Column(name = "correo")
    private String email;

    @Column(name = "tipo")
    private Integer type;

    @Column(name = "rol")
    private Integer role;

    @Column(name = "estado")
    private Integer status;

    @Embedded
    private AuditData auditData;

    /// <editor-fold defaultstate="collapsed" desc="Getters y setters">
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getType() {
        return type;
    }
    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRole() {
        return role;
    }
    public void setRole(Integer role) {
        this.role = role;
    }

    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }

    public AuditData getAuditData() {
        return auditData;
    }
    public void setAuditData(AuditData auditData) {
        this.auditData = auditData;
    }
    /// </editor-fold>

}
