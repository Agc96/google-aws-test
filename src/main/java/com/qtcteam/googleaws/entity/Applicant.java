package com.qtcteam.googleaws.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad de base de datos que representa una empresa postulante a uno o más
 * proyectos.
 * @author Anthony Gutiérrez
 */
@Entity
@Table(name = "con_postulante")
public class Applicant implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_postulante")
    private Integer id;

    @Column(name = "nombre_postulante")
    private String name;

    @Column(name = "estado")
    private Integer status;

    @Embedded
    private AuditData auditData;

    /// <editor-fold defaultstate="collapsed" desc="Getters y setters">
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }

    public AuditData getAuditData() {
        return auditData;
    }
    public void setAuditData(AuditData auditData) {
        this.auditData = auditData;
    }
    /// </editor-fold>

}
