package com.qtcteam.googleaws.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Representa los datos de auditoría, que indican el usuario, terminal y fecha
 * en la que se creó y/o modificó por última vez un registro de base de datos.
 * @author Otto Theo, Anthony Gutiérrez
 */
@Embeddable
public class AuditData implements Serializable {

    @Column(name = "usuario_creacion")
    private String createUser;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "fecha_creacion")
    private Date createDate;

    @Column(name = "usuario_modificacion")
    private String updateUser;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "fecha_modificacion")
    private Date updateDate;

    /** Constructor por defecto requerido por JPA. */
    public AuditData() {}

    /**
     * Constructor que especifica el usuario y fecha de creación de un registro
     * de base de datos.
     * @param user Nombre del usuario que está creando el registro.
     */
    public AuditData(String user) {
        this.createUser = user;
        this.createDate = new Date();
    }

    /**
     * Especifica el usuario y fecha de modificación de un registro de base de
     * datos.
     * @param user Nombre del usuario que está modificando el registro. 
     */
    public void update(String user) {
        this.updateUser = user;
        this.updateDate = new Date();
    }

    /// <editor-fold defaultstate="collapsed" desc="Getters">
    public String getCreateUser() {
        return createUser;
    }
    public Date getCreateDate() {
        return createDate;
    }
    public String getUpdateUser() {
        return updateUser;
    }
    public Date getUpdateDate() {
        return updateDate;
    }
    /// </editor-fold>

}
