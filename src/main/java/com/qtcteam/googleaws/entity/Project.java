package com.qtcteam.googleaws.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidad de base de datos que representa un proyecto sometido a un concurso.
 * @author Anthony Gutiérrez
 */
@Entity
@Table(name = "con_proyecto")
public class Project implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_proyecto")
    private Integer id;

    @Column(name = "nombre_proyecto")
    private String name;

    @Column(name = "descripcion")
    private String description;

    @Column(name = "estado")
    private Integer status;

    @Embedded
    private AuditData auditData;

    /// <editor-fold defaultstate="collapsed" desc="Getters y setters">
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }

    public AuditData getAuditData() {
        return auditData;
    }
    public void setAuditData(AuditData auditData) {
        this.auditData = auditData;
    }
    /// </editor-fold>

}
