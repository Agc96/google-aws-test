package com.qtcteam.googleaws.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entidad de base de datos que representa un recurso de Amazon S3.
 * @author Anthony Gutiérrez
 */
@Entity
@Table(name = "con_recurso")
public class Resource implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_recurso")
    private Integer id;

    @Column(name = "nombre_recurso")
    private String name;

    @ManyToOne
    @JoinColumn(name = "id_proyecto")
    private Project project;

    @ManyToOne
    @JoinColumn(name = "id_postulante")
    private Applicant applicant;

    @Column(name = "tipo")
    private Integer type;

    @Column(name = "estado")
    private Integer status;

    @Embedded
    private AuditData auditData;

    /// <editor-fold defaultstate="collapsed" desc="Getters y setters">
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }
    public void setProject(Project project) {
        this.project = project;
    }

    public Applicant getApplicant() {
        return applicant;
    }
    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }
    public void setType(Integer type) {
        this.type = type;
    }

    public AuditData getAuditData() {
        return auditData;
    }
    public void setAuditData(AuditData auditData) {
        this.auditData = auditData;
    }
    /// </editor-fold>

}
