package com.qtcteam.googleaws.dao;

import com.qtcteam.googleaws.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Anthony Gutiérrez
 */
public interface UserDao extends JpaRepository<User, Integer> {

    User findFirstByUsernameAndType(String username, Integer type);

}
