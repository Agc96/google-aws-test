package com.qtcteam.googleaws.dao;

import com.qtcteam.googleaws.entity.Resource;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Anthony Gutiérrez
 */
@Repository
public interface ResourceDao extends JpaRepository<Resource, Integer> {

    @Query("SELECT r FROM Resource r WHERE r.project.id = :projectId AND r.applicant.id = :applicantId")
    List<Resource> findAllByProjectAndCandidate(Integer projectId, Integer applicantId);

}
