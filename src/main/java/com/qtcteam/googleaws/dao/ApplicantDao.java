package com.qtcteam.googleaws.dao;

import com.qtcteam.googleaws.entity.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Anthony Gutiérrez
 */
@Repository
public interface ApplicantDao extends JpaRepository<Applicant, Integer> {

}
