package com.qtcteam.googleaws.dao;

import com.qtcteam.googleaws.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Anthony Gutiérrez
 */
@Repository
public interface ProjectDao extends JpaRepository<Project, Integer> {

}
