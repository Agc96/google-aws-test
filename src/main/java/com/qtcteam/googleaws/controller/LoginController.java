package com.qtcteam.googleaws.controller;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.qtcteam.googleaws.dto.BaseDto;
import com.qtcteam.googleaws.entity.User;
import com.qtcteam.googleaws.service.LoginService;
import com.qtcteam.googleaws.util.Constants;
import com.qtcteam.googleaws.util.GoogleUtils;
import com.qtcteam.googleaws.util.ParseUtils;
import com.qtcteam.googleaws.util.enums.UserRole;
import com.qtcteam.googleaws.util.enums.UserType;
import com.qtcteam.googleaws.util.exception.AppException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController implements ErrorController {

    @Autowired
    private LoginService service;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("googleKey", Constants.GOOGLE_CLIENT_ID);
        return "login";
    }

    @PostMapping("/login")
    public String login(Model model) {
        // TODO: auth
        return "redirect:/resource";
    }

    @PostMapping("/google")
    public @ResponseBody BaseDto google(@RequestParam String token,
            RedirectAttributes attributes) {
        try {
            Payload payload = GoogleUtils.validateSignIn(token);
            User user = service.findGoogleUser(payload.getSubject());
            if (user == null) {
                // Crear usuario 
                user = new User();
                user.setUsername(payload.getSubject());
                user.setFullname(ParseUtils.toString(payload.get("name")));
                user.setEmail(payload.getEmail());
                user.setType(UserType.GOOGLE.getCode());
                user.setRole(UserRole.APPLICANT.getCode());
                service.saveUser(user);
            }
            return new BaseDto(false, null);
        } catch (AppException ex) {
            return new BaseDto(true, ex.getMessage());
        }
    }

    @GetMapping("/logout")
    public String logout(Model model) {
        // TODO: auth
        return "redirect:/";
    }

    @RequestMapping("/error")
    public String error(Model model, HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        Integer statusCode = ParseUtils.toInteger(status);
        if (statusCode != null) {
            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                model.addAttribute("message", Constants.ERROR_NOT_FOUND);
            } else if (statusCode == HttpStatus.FORBIDDEN.value()) {
                model.addAttribute("message", Constants.ERROR_FORBIDDEN);
            } else {
                model.addAttribute("message", Constants.ERROR_UNKNOWN);
            }
        } else {
            model.addAttribute("message", Constants.ERROR_UNKNOWN);
        }
        return "error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

}
