package com.qtcteam.googleaws.controller;

import com.qtcteam.googleaws.entity.Applicant;
import com.qtcteam.googleaws.entity.AuditData;
import com.qtcteam.googleaws.entity.Project;
import com.qtcteam.googleaws.entity.Resource;
import com.qtcteam.googleaws.util.FileUtils;
import com.qtcteam.googleaws.util.S3Utils;
import com.qtcteam.googleaws.util.enums.ResourceType;
import com.qtcteam.googleaws.util.enums.Status;
import com.qtcteam.googleaws.util.exception.AppException;
import com.qtcteam.googleaws.dto.BaseDto;
import com.qtcteam.googleaws.service.ResourceService;
import com.qtcteam.googleaws.util.Constants;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ContentDisposition;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ResourceController {

    private static final Logger LOGGER = Logger.getLogger(ResourceController.class.getName());

    @Autowired
    private ResourceService service;

    @GetMapping("/resource")
    public String list(Model model) {
        model.addAttribute("googleKey", Constants.GOOGLE_CLIENT_ID);
        // Obtener los recursos necesarios
        model.addAttribute("projects", service.getProjects());
        model.addAttribute("applicants", service.getApplicants());
        model.addAttribute("resourceTypes", service.getResourceTypes());
        model.addAttribute("resources", service.getResources());
        return "resource";
    }

    @PostMapping("/resource/upload")
    public String upload(RedirectAttributes redirectAttributes,
            @RequestParam(value = "file") MultipartFile file,
            @RequestParam(value = "type") Integer typeId,
            @RequestParam(value = "project", required = false) Integer projectId,
            @RequestParam(value = "applicant", required = false) Integer applicantId)
            throws AppException {
        // Obtener los datos requeridos
        String filename = FileUtils.getFilename(file);
        Project project = service.findProjectById(projectId);
        Applicant applicant = service.findApplicantById(applicantId);
        ResourceType type = service.findResourceType(typeId);
        // Crear un nuevo recurso en la base de datos
        Resource resource = new Resource();
        resource.setName(filename);
        resource.setProject(project);
        resource.setApplicant(applicant);
        resource.setType(type.getCode());
        // Guardar archivo del recurso en Amazon S3 y en la base de datos
        S3Utils.upload(file, service.makeResourceKey(resource));
        service.saveResource(resource);
        // Mostrar mensaje de éxito
        redirectAttributes.addFlashAttribute("alert", new BaseDto(false,
                "Recurso subido correctamente."));
        return "redirect:/resource";
    }

    @PostMapping("/resource/delete")
    public String delete(RedirectAttributes redirectAttributes,
            @RequestParam("id") Integer id) throws AppException {
        Resource resource = service.findResourceById(id);
        // Eliminar el recurso de Amazon S3 y de la base de datos
        S3Utils.delete(service.makeResourceKey(resource));
        service.deleteResource(resource);
        // Mostrar mensaje de éxito
        redirectAttributes.addFlashAttribute("alert", new BaseDto(false,
                "Recurso borrado correctamente."));
        return "redirect:/resource";
    }

    @GetMapping("/resource/{id}")
    public @ResponseBody byte[] download(@PathVariable Integer id,
            HttpServletResponse response) throws AppException {
        // Descargar el recurso desde Amazon S3
        Resource resource = service.findResourceById(id);
        byte[] data = S3Utils.download(service.makeResourceKey(resource));
        // Actualizar headers
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        ContentDisposition header = ContentDisposition.builder("attachment")
                .filename(resource.getName())
                .build();
        response.setHeader("Content-Disposition", header.toString());
        return data;
    }

    @ExceptionHandler(AppException.class)
    public String handleError(AppException ex, RedirectAttributes redirect) {
        redirect.addFlashAttribute("alert", new BaseDto(true,
                ex.getAlertMessage()));
        return "redirect:/resource";
    }

}
