jQuery(function ($) {

  $('#type').change(function () {
    var type = parseInt(this.value);
    switch (type) {
      case 1:
        $('#project, #applicant').prop('disabled', false);
        break;
      case 2:
      case 4:
        $('#project').prop('disabled', false);
        $('#applicant').prop('disabled', true);
        break;
      case 3:
        $('#project, #applicant').prop('disabled', true);
        break;
    }
  });

  $('#modal form').submit(function () {
    var type = parseInt($('#type').val());
    if (type === 1 || type === 2) {
      var project = parseInt($('#project').val());
      if (!project) {
        showErrorMessage('Seleccione un proyecto de la lista.');
        return false;
      }
    }
    if (type === 1) {
      var applicant = parseInt($('#applicant').val());
      if (!applicant) {
        showErrorMessage('Seleccione un postulante de la lista.');
        return false;
      }
    }
    var files = $('#file').prop('files');
    if (!files || !files.length) {
        showErrorMessage('Seleccione un archivo para subir.');
        return false;
    }
    var size = files[0].size;
    if (size > 52428800) { // 50 MB
      showErrorMessage('No se puede subir el archivo ya que es mayor a 50 MB.');
      return false;
    }
  });

  $('[data-target="#delete"]').click(function (e) {
    $('#id').val($(e.target).data('id'));
  });

});
