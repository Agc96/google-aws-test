/* global iziToast */

function showMessage (type, message, icon, title) {
  iziToast.show({
    class: type ? ('iziToast-' + type) : '',
    title: title || '',
    message: message || '',
    animateInside: false,
    position: 'topRight',
    progressBar: false,
    icon: icon || '',
    timeout: 3200,
    transitionIn: 'fadeInLeft',
    transitionOut: 'fadeOut',
    transitionInMobile: 'fadeIn',
    transitionOutMobile: 'fadeOut'
  });
}

function showErrorMessage (message) {
  showMessage('color-red', message, 'ico-error', 'Error');
}

function showSuccessMessage (message) {
  showMessage('color-green', message, 'ico-success', 'Éxito');
}
