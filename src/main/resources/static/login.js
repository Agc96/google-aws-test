/* global gapi */

function googleSignIn(user) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/google', true);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function () {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      if (xhr.status >= 200 && xhr.status < 400) {
        var data = JSON.parse(xhr.response);
        if (data.error) {
          showErrorMessage(data.message);
        } else {
          console.log(data.message);
          window.location.href = '/resource';
        }
      } else {
        showErrorMessage('Hubo un error desconocido en el servidor.');
      }
    });
  };
  xhr.send('token=' + user.getAuthResponse().id_token);
}
